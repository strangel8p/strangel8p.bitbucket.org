window.hypothesisConfig = function () {
    return {
      openSidebar: false,
      enableMultiFrameSupport: true,
      onLayoutChange: function(state) {
        var main = document.querySelector('#main');
        if (state.expanded === true) {
          main.classList.add("open");
        } else {
          main.classList.remove("open");
        }
      }
    };
};