// https://dl.dropboxusercontent.com/s/56ce1pll7cf5nzg/a.epub

var params = URLSearchParams && new URLSearchParams(document.location.search.substring(1));
var url = params && params.get("book") && decodeURIComponent(params.get("book"));

// var hashUrl = window.location.href.indexOf("/#/");
// if (hashUrl > -1){
//     var url = window.location.href.slice(hashUrl + 3);
// }

var book = ePub(url);
var rendition = book.renderTo("viewer", {
    flow: "scrolled-doc",
    ignoreClass: "annotator-hl"
});

// var hash = window.location.hash.slice(2);
var loc = window.location.href.indexOf("?loc=");
if (loc > -1) {
    var href = window.location.href.slice(loc + 5);
    var hash = decodeURIComponent(href);
}
rendition.display(hash || undefined);


var next = document.getElementById("next");
next.addEventListener("click", function (e) {
    window.scrollTo(0, 0);
    rendition.next();
    e.preventDefault();
}, false);

var prev = document.getElementById("prev");
prev.addEventListener("click", function (e) {
    window.scrollTo(0, 0);
    rendition.prev();
    e.preventDefault();
}, false);

rendition.on("rendered", function (section) {
    var nextSection = section.next();
    var prevSection = section.prev();

    if (nextSection) {
        nextNav = book.navigation.get(nextSection.href);

        if (nextNav) {
            nextLabel = nextNav.label;
        } else {
            nextLabel = "next";
        }

        next.textContent = nextLabel + " »";
    } else {
        next.textContent = "";
    }

    if (prevSection) {
        prevNav = book.navigation.get(prevSection.href);

        if (prevNav) {
            prevLabel = prevNav.label;
        } else {
            prevLabel = "previous";
        }

        prev.textContent = "« " + prevLabel;
    } else {
        prev.textContent = "";
    }

    var old = document.querySelectorAll('.active');
    Array.prototype.slice.call(old, 0).forEach(function (link) {
        link.classList.remove("active");
    })

    var active = document.querySelector('a[href="' + section.href + '"]');
    if (active) {
        active.classList.add("active");
    }
    // Add CFI fragment to the history
    history.pushState({}, '', "?loc=" + encodeURIComponent(section.href));
    // window.location.hash = "#/"+section.href
});

book.loaded.navigation.then(function (toc) {
    var $nav = document.getElementById("toc"),
        docfrag = document.createDocumentFragment();

    toc.forEach(function (chapter, index) {
        var item = document.createElement("li");
        var link = document.createElement("a");
        link.id = "chap-" + chapter.id;
        link.textContent = chapter.label;
        link.href = chapter.href;
        item.appendChild(link);
        docfrag.appendChild(item);

        link.onclick = function () {
            var url = link.getAttribute("href");
            console.log(url)
            rendition.display(url);
            return false;
        };

    });

    $nav.appendChild(docfrag);


});

book.loaded.metadata.then(function (meta) {
    var $title = document.getElementById("title");
    var $author = document.getElementById("author");
    var $cover = document.getElementById("cover");
    var $nav = document.getElementById('navigation');

    $title.textContent = meta.title;
    $author.textContent = meta.creator;
    if (book.archive) {
        book.archive.createUrl(book.cover)
            .then(function (url) {
                $cover.src = url;
            })
    } else {
        $cover.src = book.cover;
    }

    if ($nav.offsetHeight + 60 < window.innerHeight) {
        $nav.classList.add("fixed");
    }

});

function checkForAnnotator(cb, w) {
    if (!w) {
        w = window;
    }
    setTimeout(function () {
        if (w && w.annotator) {
            cb();
        } else {
            checkForAnnotator(cb, w);
        }
    }, 100);
}

book.rendition.hooks.content.register(function (contents, view) {

    checkForAnnotator(function () {

        var annotator = contents.window.annotator;

        contents.window.addEventListener('scrolltorange', function (e) {
            var range = e.detail;
            var cfi = new ePub.CFI(range, contents.cfiBase).toString();
            if (cfi) {
                book.rendition.display(cfi);
            }
            e.preventDefault();
        });


        annotator.on("highlightClick", function (annotation) {
            console.log(annotation);
            window.annotator.show();
        })

        annotator.on("beforeAnnotationCreated", function (annotation) {
            console.log(annotation);
            window.annotator.show();
        })

    }, contents.window);

});