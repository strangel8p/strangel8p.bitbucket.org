const locations = [
  {
    name: "FELCV",
    type: "felcv",
    phone: "2285384",
    tollfree: "800101545",
    position: [
      -16.4939061,
      -68.1344444
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "2821212",
    tollfree: "800142031",
    position: [
      -16.5063398,
      -68.1634477
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "4233133",
    tollfree: "800140195",
    position: [
      -17.3914836,
      -66.1925635
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "3538579",
    tollfree: null,
    position: [
      -17.7517863,
      -63.1885543
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "6453944",
    tollfree: null,
    position: [
      -16.5063398,
      -68.1634477
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "6643333",
    tollfree: null,
    position: [
      -21.525692,
      -64.731316
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "6226550, 2242099",
    tollfree: null,
    position: [
      -19.5794063,
      -65.7641074
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "5284512, 2-5250899",
    tollfree: null,
    position: [
      -17.968653,
      -67.1168894
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "4624875",
    tollfree: null,
    position: [
      -14.830647,
      -64.905413
    ]
  },
  {
    name: "FELCV",
    type: "felcv",
    phone: "8421133",
    tollfree: null,
    position: [
      -11.0358373,
      -68.7695971
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "2406100",
    tollfree: null,
    position: [
      -16.49607,
      -68.1356752
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "2821120",
    tollfree: null,
    position: [
      -16.5130035,
      -68.1643214
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "4504626",
    tollfree: null,
    position: [
      -17.3860873,
      -66.1546797
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "5250034",
    tollfree: null,
    position: [
      -17.9672879,
      -67.1170717
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "6223878",
    tollfree: null,
    position: [
      -19.5881151,
      -65.7554807
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "6453640",
    tollfree: null,
    position: [
      -19.0429841,
      -65.2660552
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "6642649",
    tollfree: null,
    position: [
      -21.5317324,
      -64.7361296
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "3346865",
    tollfree: null,
    position: [
      -17.7711237,
      -63.1843113
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "8424014",
    tollfree: null,
    position: [
      -11.0142445,
      -68.7546964
    ]
  },
  {
    name: "Fiscalía Departamental",
    type: "fiscalia",
    phone: "4621444",
    tollfree: null,
    position: [
      -14.8363243,
      -64.9101884
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "3463104",
    tollfree: null,
    position: [
      -14.827750,
      -64.895257
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "4586818",
    tollfree: "800140205",
    position: [
      -17.3928344,
      -66.1507091
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: null,
    tollfree: "8001041000, 156",
    position: [
      -16.4930495,
      -68.1478307
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "5250943",
    tollfree: "156",
    position: [
      -17.968037,
      -67.116557
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "8423486",
    tollfree: null,
    position: [
      -11.0289576,
      -68.7605646
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "6229164",
    tollfree: null,
    position: [
      -19.5923643,
      -65.7526764
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "6458682",
    tollfree: null,
    position: [
      -19.0393265,
      -65.2654435
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "3355703",
    tollfree: "160",
    position: [
      -17.794956,
      -63.1754579
    ]
  },
  {
    name: "Servicio Legal Integral Municipal",
    type: "slim",
    phone: "6672031",
    tollfree: null,
    position: [
      -21.5258088,
      -64.7375515
    ]
  }
];

const icons = {
  fiscalia: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIhAAACIQBDVcC+gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAfhSURBVFiFtZh7bBTXFYe/e3d27bWNAeOADTYP87SBkmAwIKFKPEQgjamIahJKUJO0JSHQNiKNIjW0caKGVkUtiahpkka0SNACjkgDKSEUAQ1FCWDe4IKd4Cd+sF78WO97Zm7/MH7umh1Dev6aueec+/vm3jt3zh3BfVhBgbJ5qMpTggUolYtgCpAOJN0NaUdRB9xAiHNSqGNzsseeKSwU5kC1xECClzz1VaYwbesVPC1g1AClaoVQu0RYL/p0/8TabxRwWUH5Q0pqv1aKZwDHwMAiLATscNhCvzz498lNDwz4aEHl9xFqG5DygGB9za0EG47sHbfnXkH9AuauLbGntqRuB/Wjbxislyl4zz3E/ZNz788KR/NHBczPr0sIxQc/RLDs/wnXaQoOxfnjCg4eHOnr64sAzF1bYk9tHvaxVThlGrS5L9HWXErA14AydQR2NGcqaaOX4ExMt4gpjiYr73eKi6eGerZqfcNSW1K3I5QFOIWr9ih1N/cTDjZHehXcrj6KM2ks2bM3YncMjdXf4jaR8A6wrhd2z5slT1auFkrtioVmmiG+vvQHWpsu9mrPypAA3Kw1e8QKDNNB9qyXGfLQt2J1jVI8daR43N7Oe9l5sWhF7TCheDt2B2ZUuIWzNA5tHcShrYNYNLt7YqRU2GSI0rNb8HlqYgIKwfb8VTdSIwDtjvBmUKnR07rNVfuvXnCPTNLY+XoC+36TSJMviyb/OPZuTmT3m4nk5di6IIXQKT37u5iAQErQsL/RBQyw7InyDFPTvibGJmwaQS7/ZwOjUry8+L045k63MTWrA+JKzUS2HHoOgFce28H0zHIAyqpNTl3S+eO+IDeqIGf2RlJGzIoFGTKUOfFo8fhqCWBq2oZYcAAtrhL0kIfxGZIfftfRBQcwPbOcyWkVTE6r6IIDmDRa8my+g/EZEiEU9ZWHY8kAODQp1wFohYVKflFatRpUzCxP838BiHcOpqJxMFIoNM0AwFSS5769D4Aa91Dk3bpA123Ea3bGpAeRopqAt9EKIKZiTUGB2qR9ea1qDkJlWEkK+OoB8IaSGTGuwJIQgKemHYfjNIhqdCNoKUfAqDYqcqUSLLAqpIfbLUNFF1UoZb3iUoiFUqFirtieEg9mAqFiL6VOk0LkSqGYZLl7KWMHxTBlYa13momaLBFY/Vgi1IONoBBgGrr1eES6pLtMtyDwYCMoBJimjlKGxQw1aECKeth7H1jdJoRCKfB5qiznSMDyqxkKuAFo94ZiRPYxQ3Gn1YcQHaPoqvvCYqLwSBT1VkKDfhem2bGHXb5ez223tecyDZNwUOeTk2UA2KSi1XXFUq5C1UslKLMS3Oa+1HUd1k227bQ2Cv7bAY5ersQf7Hg5pIT2tlpMM/bLIhE3pECUWBFy133e637XPy5w+uK9y6ewT6el2ccvio52i9oUpmnSUHkopqZClUgp1LFYgc23T9Pe2nugDUOx/lcHaG71R80xwybeRh/Pbj5AKNz99RCAXVPUfHUwJqAQ5jE5J3vsGaDfoWi7c5XKq3+K6rvtbmfXRxci2o2ggbfBx/UaF9crXBF+m00RDnmpq/jnvfiq52ZnnZOFhcIUgt19vQHvLcrOv0X5+c0YRqDfXlo93T5lKELtYXS/jpag0dYevTAQAjRNUXm9uN+1qGB3YaEwJYAI60V0nPi7LD5xFM6kMagBfDuFTeBIshM3JC7mV1uzKUwjxM0rH0RzBx1KK4K7Jf/dfyU7+kZlTnqaybmbkLY4y5ADMYdd0VBzkpDf3dshxJ8/Kc681QUIgNJeA9EEHQejgK+OhsoDVFwtwoxSw40akcyq5TNYsXRavwDTJozglR/MZ/qEEVH9UioEipulf+3Z7HbIYO8zSaflPFy4xV33+c8N3R+1bktMcLB80RSeWDqNvBkZCNH/RPqbOtamMzWekD9MWZmLU+eref/DM7haut98XRcgncx9tGMChVArD+/NKo4KCJA18YVrrU0Xcnq2OePsPFMwk+dX5ZEyJKFfKOg4sBt+nZAnTNzQOGyOHpNkKpobvBw/XcGb7x2n2ePHVBAMSvIWbyPOmVL02b6sDT37i/iz4HQsfEQf7K3ytpalASQ67RQXrWba5OjT1BcOZaIlaGgJEV0jpCBlZBIrHp/KvCkjefzlv+Fq6fgdEwo2nsTjealvTkQ1c+3aylDmqOcnDRmeVw3w4pp5veBKypKpbHRGBRSi/6K2stHJ+fLBHaJ2ybD0JLZuXIpSgrj4pEASGctOnFgQsedEPiZw6tR8T+7akgnDyubvzV8cWNFT5Ld7soh3mDyW5yJ3Yhvj0vw47NHPGcGwpKLBybmyZA6dGU4oLPj9C9cZPTyAI9lBTmYq9viR9QnO5KwjR2ZE3Wxjlsgf7fz32/Ny7vx0UKIS2z4ew/ELvf9j2qQiK93P0tkuFjx8B4BjF4fx2dlUbtY7MczeEotmulm/vJo7LX5OnGy6vObVn824l76lGv4v7345MSvNu3/H4YxpDXf6P9+vy69GIXj3YGa/MSOHBcjPPVNb7R6++KVXl92IpT2gQ8Zbb5xf5gnaPqhsdI5s9tgj/OPSfICgoiFyjQ4dFGbscP+tQfHGj197feanVjXv6xS0b981R2lpcFMwbHvSF9QyW9pt8R6fTSgESoEUiuQEQw1OMgKJ8Xp1nKbvyc6J37xy5dQBluLwP1kPM9+UBTlTAAAAAElFTkSuQmCC",
  felcv: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIhAAACIQBDVcC+gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAe7SURBVFiFtZh7bFPXGcB/5/j6OnEcyHgUEgKEZxmlvAJ01appPDRBV1rBGlZgfaBuTKNs2roHU9euXlcB06SVqQ2jdOoKJW0hqO0KzTQNAYIxCuOZAYNUBeKEPEgcJ7Hjx7XvPfvDkODYwTel+yRLvuf7vvP9fPyd73znCj6HlO0qc5CfO0cp5qIoRYhJoAoBzw2TENCA4hKCkxZy/9TjJce9Xq/V31iiP8aP/X3VyIRpPgN8BxjRz1j1SrFDCbP8/Qcr6r9QwLKqVUMtEi8LxFOA3k+w3mKAeNNpGi+8u/jd1jsGLPv4iRVK8Cow6A7BeosfodbuXvT2e7cz6hNw9YnVzraW6GYU3/2CwVJF8fqgYTk/3DprazyTOiPg4j2r3S5HdDew6P8K1wNRFTVzyvYs3hrOoEuV1SdWO9uuR/9qF85KKAL/6SJwIUy4KY6VUEhN4hysMeobBeQV2UtZodhHV+SblcsqjVvHtd6GbS3RzbbgFDTsb+fqB35igUS6WkHDP9rJG5XLzHWF6AVpoVLtBQvwuP8I/CAF/NaHR6ueXAlqRzY2K64498o1/Ge7UsbHFksALtf3lDvLElhSMvMXhQyelpdtagQ8Vvng9p03n+XNL0v2PTEY1KZsEyiTjHDzZmlUbfJQtcnD/Nk9qyWlQloWJzY0EKqNZQVUsHn5nuVD0gClodYDQzJ63SIN+9tT4GZM1Nj2optdG/Joc7sIuHV2rs+j4qU85kx2dEMKpTi5oSErIDAoLp2/ufkgAJZWrSyWOD4jSxE2o4qjP/mMIrdizaMuvnKvg3vGJiEuGDqvBQsAWJvfzmQ9mes1PosjZxO8tivGpVooXVfEXbOz/tWGMJlQuXi7TwJIHGuzwQH4TwWJd5qMK5Y8/YjeDQcwWTcYryU/N+EAJo6SrFqsM65YIoSitiqQLQyAbsnkZtG8Xq88x+WVdrwCFyMA5OQO5ErzQKRQaJoJgAWs0JK7ua5tYHfuJBIOcjQnowtjSOEj3JyxHqeJEDxetqvsee38fZfvQ1FsxyncmFyZLmMAw8aU2QoEEKwLoevHQPhIRJVdtxEMyC2VSjHXrke8y7QNlUkECmXZBgTFPAnMsh+gX91ZxhnoD5+iVAITbU/vyG6TPWq/rO+WQKFda3GHCygEmEa/CAslPW26vQh3IEIkm4t+5GG+zG7TI4k73SRCoRQEbRx5N0WSvODYkqg/WedCXUYWy15iKto6wgiRXMXGI0G7nkEJNNqxjLTEsYxkl1J9sZHrfnu/yzIt4rEEew/XAOCQCv+ZtL60L2mUQI0dy0B1T4MQT1i8uu2orQiR61H2VV8lEkuuvpTQ6TOwTFt5eEkCJ+xYNh3uTHne8eFpjp2pu61PPJygPRDmufJ93WPSobBMqN3bbifsCWkh92ezavl3kI5PIyljpql45tcfEeiIZPSx4hZdzWFWrf8II97TwArAqSkuf9iWlc4Scr+cerzkuII+lyJwLsx/tzRl1F33h9jxwem0cTNm0tUU5mJdCxevtKTpHQ6FEbS4uve2nY1v6rGSk9Lr9VooKnpruxoMzmyo5+zGesxo328sOoLR7u/KVBihOIlIAs2t0RnKXE6EAE1T1Lzj7zsXharwer2WBFDCLAdSakdekY5ntAvVj7NJOAS6x4mrwJX11NYcCtNQnN/SnEkdEwlRDjda/uS7EvFmb6vxK4Yy47mROHLutEnILLpTUX8wSMyf2iMqeKNy8fZr3YAAQmq/AloheTEKNxj49rRx4U+NmBl6uBHDBrD84WksWTilT4Ap44fx8ycf4N7xwzLqk3cVuPCXlDz162Y89U5yU6avWPj7pkMdP0tELFSGtMtz6zw8fxJLF05hzrRixG3O5khrMjdzh+RgROLU1LRw5JSPrbuP09Les/MTCYFwSRZsH3+DSC3bvejtyoyAABPmzj/fdiY0+daxXJeTp8pm8v3lcxhU4O4TCpIXdjOSwAjGcX3JhUPvOe6VpQg0dXHg2BVeev0AgWAES0EsJpm3pYScu/Ty3Yu2rb11vrTrvj5k0IwBE8zazk8jwwHycp1Ulq9kyt2Z/6becCgLza2hudPfJAgpGFTkYclD93D/pCIe+uk7tLQnj71Iq3E4NKrxx7190rqZ85WVRtG3xk4cOjvfB7Dm8ftT4M6Em/DFOzICCgFCZm6QfPEOzkaSO1Y6JYMLPbzy7EKUEuQUyKhjqHvRwbkH096hZOyR6/adNpbuXFDuGKxNeXbeV79c4MnpDrKx+QhHQ3VELRNdOvBIHYfIDBVTJldiAQ4Er/KWv5p/heoodRcywOHCoTvIS0i2Hz3X6J5bX3TyxUMZj6Ss9ePjT/6wqTS38Ef5Tpd4o/U0/wzV9vqFktH6QOYPGMMDnlEAHA752N95hVqjA5PU3fY1TwlPD5lOW3uEQ6cuV69c+8K028W3VeAqPvnthBJtyPsVgeop1+NdfdqtGjwdJeCt1jN92gzXPMzvHFPf6OxYsOaRly9li92vCrzx4LpFUdP6c32io6jdjKbpR+sDUYDPSM/RAkcOI/X8ay6hfe+XX//d3+zG/FxHxK5dXv3KcOP5uEp8O2QaI4NmLCdkGkIBSoBU4JG6ytdc0Tzh9GnS+d64Zn39smXefrbi8D9HtSNmVtgLaAAAAABJRU5ErkJggg==",
  slim: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAIhAAACIQBDVcC+gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAfDSURBVFiFtZhrcFVXFcd/e99zn7mEkoSmCUFJaBKEUAbC8NBOHWiHklpw2jGtlVLLqCgPHaxaHcUaOkqd9oNKTbFYO8NTSrDaAiIMUIaWFmgoNLYMCVCSEBLyuCQkue9zz/bDhYT7SO65Bf+fcvde+6xf1t5n7bWO4HNIVVZampx9MzDUHCUoByYAeYD7ukk/0ArUC8VJpeShccUzT4iqKiNdXyId4/NL5o21RMQKlHoSxJg0HbUoxBbdENXFW/e23FbAc0sqRmu6+i2opwFbOmBJFAJeD+m2X5f+fVfXLQM2PvXgt5TiZSDrFsHi5QG1snDz/u3DGQ0JWLu03JoTyHlFKb57m8FipdSrHpfnh9M3nAwnm04K2Lp0gSvoC+1EUPF/hRuQ+rfdaa/M37DLFz+TAFi7tNya7ct5yyycbsCxzjC1Hp0mr4FugE3AnQ7J40V2xrmlKUQBB7yBzK9NqqkJ3TyuxRvmBHJeUSbgFPBmU5DXGgJ0BlXivIKdjSGKR2ism+0ixzH8cVfwgMvR+ydgWRz4oBqfnLdICbElFVzQUDxb6+Vohx4zXlQQjdZnLYPpzjAEFiVYN8vF7NyEeCRKqG8Wbtr/RgJgy1P3Z+tKO6sgZ7j1EQXPfNifADd3usb6n7sAWP6ij4MfDs4bhsCICGrmuikemXLLr4Z0W+mNFDRgHVbWtangAP7RFIyBm1qisfE3Lna8kIHbL3EHBG+szWDr8xnMmGgBQEqFEooV73tTPR4gy2oJrrnxQwCcW1RRoEnjAimSsD8CCw5dY+QowfJv2Jk12cKkoihE4DJ07Y9uSM48heP6PdPQbHD0Y50/7whS3wTrvuxibl7KrQ4ZIlI8ftOBZgmgWYyVqeAAjrSH6AkqxhdIvvN12wAcgGMM2HMV9txBOICSL0iWLLAxvkAihGLb+WAqNwA2qbRlAJqqqpJNFz5YlPgeJuqkJxKFcY7kYvtIpFBoWnQMBfbZ0a2/7NEGTreuW3BoVr6YF0SKZi55zdYLxmJVWblaa/rs2EwFBWaWNHujMN5QJrmFlUPaueJ+913qx2Y7DqIZr550SRKJMRedveUSQ80xu+RayEych3GJwkjjEQI1VyrUdNML0irOkruMpniT1oYslwhRYnaB5ZYBSSuCShqlkmglbErmbtWhJQSEImnYK5EnGSzTU0qmV4AnOhQQMiCizIVRwYi0gtIbvsWXRCiUgoZe82GURBscU2oPRHNYvzeUwjJOEcXVaz6EiEZx7yVzuUZAnwTazBi3+gwCkWgE68620eEx938ZEYNwUGf3uw0AWKTi/XZzgEqoNolSDWaMj3UOVuRh3eDljR+YcuLvCHCgrhF/MAolJZy7ZhA28ToLQ9ZLgag142h3S2zLsOVfpzh++tKwa8I+nZ5uH7+sPjAwJi0K3YAtF1IfE4WqlUrJQ6kMD14JU9cduy2RiGLFc2/Tfc2fdI0RNvC2+1iy9m1C4cH7VwBWTfG3s6mLBkOJQ3Jc8cwTwJChONGls+Z0Qi8DQIenny3/PJUwHglG8F7xcfZSJ2cvdibMWyyKnhBsGr6yaS4qnnVSRj9HiK3xsxf7DZYf87LyuBefPvR5udYXGPhbRRSh/jC6X0dzafT2JwcQAjRNse6T4DBnUW0VVVWGBNANUU204x9QoVtSkilRJpMqgLAIbG4r9jvsKVO6ZlEEIrDmVCDZdFATtmq4fntd/1byerzVqolO1s9y47TEz9we2ayKtxrDtPtja0Qh+OvYTbsvDwACaDZ+JaALoo1RY7/BxgtBnjvtw58k8Y/JzeSJhVN4ZH7ZkABld+fys2/fy+S7c5POSxndnRfqYqLoCYZtsT3JDa2cdt9Le1pCP/XqikiSnc1w2Vh4/wQenV/GjCkFiGHqL39X1Kkzx0HIH6ahoZOjHzWzYecJOnsG33xdFzgsgmMLMgFQiMeKNv+nJikgwMKSez892hmeePOY027l6cppfP+JGWTdEV8vx0opiPh1Qn1h7KPsWGyD170yFN1XvLxz/CLPv/oO3X1+DAXBoOTgQ25yXbK6aPO+lTc/L6G9mmbNm9o7qrXpv936XQAZTis11YsoK02+TfFwKAPNpaG5Ejs3IQVZ+W4eeXgSsyfk8/BPttHZE01hlwORdwOe8Kr4NQnVTNWnNaEfl2WXzLnL2gywfPHsGLjg+Sb0Dk9SQCFAyOQFkt7hIXQhmm6lVZKd5+YPz8xHKUG2k0BxllYx5/DhhEs66fu5vb4+tOnR0dX3uEaUPbh47pcyM50DTnq27SJw5jwqpCOsGtLpRFiSQyldR2/twH/qDH373iPwSQP20kJkhhOLzUKGLtmz7+O2r2S05v/gyMmkV1LKCrRr6+/+aC0t/JHmcojePYcJ1NXHGkiJNTcHZ/lEHJNLAQjU1eP/6Azh9i4wYlOIY8oEMh/6Kld7/Jx+71zdgmerpgzn31SJ3PGX1cVafs6b/QeOlkW6e4e0G1FxHwB9e48MaWPJGknrPVNbnH3dD0xZ9WL9kIbpAN5Q2++XVRi+8Gt6pyff6E+8n7XcbFACvSPx07N0u9BGZ1+WLuv38n6xfq9Zn5+ryVA7qmxt5zpWo4ceN/zhsYbf5zC8QRFtKRUgkS6Hki5nQDjtzUhte37pnWvFY1VpluLwP1b4LRvVnISKAAAAAElFTkSuQmCC",
};

let map;
const create = function createMap() {
  map = L.map('map').setView([-16.341642,-64.839907], 6);
  L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
    attribution: 'contribuidores &copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
  }).addTo(map);
};

const place = function placeOnMap(locations, icons) {
  locations.forEach((location) => {
    const lat = location.position[0]
    const lng = location.position[1]
    const png = L.icon({
      iconUrl: icons[location.type],
      iconSize: [40, 40]
    });
    const popup = []
    popup.push("<b>"+location.name+"</b>")
    if (location.phone) {
      popup.push("<br>Teléfono: ")
      popup.push(location.phone)
    }
    if (location.tollfree) {
      popup.push("<br> Línea Gratuita: ")
      popup.push(location.tollfree)
    }
    L.marker([lat, lng], {icon: png}).addTo(map)
      .bindPopup(popup.join(''))
  })
};

create();
place(locations, icons);
