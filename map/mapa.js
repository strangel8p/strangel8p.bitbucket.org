const person = L.icon({
	iconUrl: 'icon.png',
	iconSize: [36, 36]});

function locationFound(e) {
	console.log('location found');
	L.marker(e.latlng, {icon:person}).addTo(mapa);
};

function locationError(e) {
		alert(e.message);
	}

const places = (mode, mapa, location=[]) => {
	let tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
	});
	tiles.addTo(mapa);
	if (mode == "manual") {
		L.marker(location, {icon:person}).addTo(mapa);
	}
};

const display = (mode, location=[]) => {
	if (mode == "manual") {
		mapa = L.map('mapa').setView(location, 17);
		places(mode, mapa, location);
	} else {
		mapa = L.map('mapa').fitWorld();
		places(mode, mapa);
		mapa.on('locationfound', locationFound);
		mapa.on('locationerror', locationError);
		mapa.locate({setView: true, maxZoom: 17});
	}
};

const init = () => {
	const location = window.location.search.slice(1);
	if (location != "") {
		display("manual", location.split('&'))
	} else {
		display("automatic")
	}
};

let mapa
init();
