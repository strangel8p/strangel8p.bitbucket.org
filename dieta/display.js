const element = (tag, properties = '') => {
  const el = document.createElement(tag);
  if (properties) {
    Object.keys(properties).forEach((prop) => {
      if (typeof properties[prop] === 'object') {
        Object.keys(properties[prop]).forEach((p) => {
          el[prop][p] = properties[prop][p];
        });
      } else {
        el[prop] = properties[prop];
      }
    });
  }
  return el;
};

const showDia = (text) => {
	n = element('div', {'className': 'dia', textContent: text});
	dieta.appendChild(n)
}

const showComida = (clase, titulo, comida) => {
	n = element('div', {'className': `${clase} comida`});
	t = element('div', {'className': 'titulo', 'textContent': titulo});
	c = element('div', {'className': 'contenido', 'textContent': comida})
	n.appendChild(t);
	n.appendChild(c);
	dieta.appendChild(n)
}

const showDieta = (dia, data) => {
	comidas = ['ayunas', 'desayuno', 'mañana', 'agua', 'snack', 'sopa', 'almuerzo', 'tarde', 'cena'];
	showDia(data[1][dia]['text']);
	comidas.forEach((comida) => {
		showComida(comida,
							 data[0][comida],
							 data[1][dia][comida])
	})
}

// const showDia = (dia,text) => {
// 	d = element('div', {'className': 'dia', textContent: text})
// 	d.addEventListener('mouseup', showDieta(dia))
// 	diasHolder.appendChild(d)
// }

const showMenuItem = (titulo, dia) => {
	return element('div', {'className': 'menuitem', 'textContent': titulo, 'dataset': {'dia': dia}})
}

const menuHandler = (e, data) => {
	document.querySelector('.menu').remove();
	showDieta(e.target.dataset.dia, data);
}

const showMenu = (data) => {
	dias = ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo'];
	menu = element('div', {'className': 'menu'})
	dias.forEach((dia) => {
		item = showMenuItem(data[1][dia]['text'], dia);
		item.addEventListener('mouseup', e => {
			document.querySelector('.menu').remove();
			showDieta(e.target.dataset.dia, data);
		});
		menu.appendChild(item);
	});
	dieta.appendChild(menu);
}

const show = (data) => {
	// dias = ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo'];
	dia = window.location.search.slice(1);
	if (dia == "") {
		showMenu(data)
	} else {
		showDieta(dia, data)
	}
}

const init = (url) => {
	fetch(url, {
    method: 'GET',
    redirect: "follow",
    mode: "cors"})
		.then((response) => {
			return response.json()})
		.then(show)
}

const dieta = document.querySelector('#dieta');
init('dieta.json')
